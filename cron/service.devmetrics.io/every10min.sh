#!bin/bash

#HeartBit  service.devmetrics.io  
#jsonstr='{"level":"info","message":"{\"app_id\":\"sys-heartbeat\",\"event_type\":\"user_event\",\"host\":\"localhost\",\"message\":\"user event: HeartBeat of logstash-elasticsearch service\",\"version\":1,\"severity\":\"info\",\"uri\":\"N/A\"}","label":"sys-heartbeat"}'
#echo $jsonstr | nc localhost 5545

#Check that elastic search receives events
node /home/ubuntu/projects/service/alerts/check_service.js "app-sys-heartbeat*" "false" "service.devmetrics.io"
status=$?
if [ $status -ne 0 ]; then 
	logger "Logstash has not received heart beat events and will be restarted"
	sudo pkill -9 -u "logstash"
	sudo service logstash restart
	sleep 60
fi

#HeartBit  localhost  
jsonstr='{"level":"info","message":"{\"app_id\":\"sys-heartbeat\",\"event_type\":\"user_event\",\"host\":\"localhost\",\"message\":\"user event: HeartBeat of logstash-elasticsearch service\",\"version\":1,\"severity\":\"info\",\"uri\":\"N/A\"}","label":"sys-heartbeat"}'
echo $jsonstr | nc localhost 5545

