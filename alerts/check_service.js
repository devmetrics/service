/**
 * Install:
 * npm install elasticsearch node-slackr
 *
 * Run by cron:
 * node check_service.js
 * optional parameters: 
 * indexpattern="app-token-123128*", sendNotification="true", server="service.devmetrics.io"
 */

var MAX_SECONDS_TIME = 15 * 60; // 15 mins
// var MAX_SECONDS_TIME = 10; // 10 secs for test

var indexPattern = 'app-token-123128*';
var sendNotification = true;
var server = 'service.devmetrics.io'

process.argv.forEach(function(val,index,array){
    if (index == 2){ indexPattern=val};
    if (index == 3){ 
        sendNotification = new Boolean(new String(val).toLowerCase() === "true");
    };
    if (index == 4){ server = val;};

});

    // if (sendNotification==false){
    //     console.log('kuku');
    // }
    // console.log(indexPattern);

var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
    host: 'amazon:123Secret123@'+ server + ':5550',
    log: 'info'
});

Slack = require('node-slackr');
slack = new Slack(
    'https://hooks.slack.com/services/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',
    {
        channel: "#site_contact",
        username: "slack-bot",
        icon_emoji: ":ghost:"
    });


client.search({
    index: indexPattern,
    size: 1,
    sort: "@timestamp:desc"
}).then(function (resp) {
    var hits = {};
    var curDate = new Date().getTime();
    var lastLogDate = 0;
    try {
        hits = resp.hits.hits;
        lastLogDate = new Date(hits[0]['_source']['@timestamp']).getTime();
    } catch (err) {
        console.log(err);
        slack.notify({'channel': '#dev', 'text': server + ': problem with fetching result from elastic'});
    }

    var timeDiff = (curDate - lastLogDate) / 1000;
    // console.log ({'Date Parse': new Date(hits[0]['_source']['@timestamp']),'curDate': new Date()})
    if (timeDiff > MAX_SECONDS_TIME) { // 15 mins
        console.log({'channel': '#dev', 'text': server + ': no events for 15 minutes, lastest minutes back: ' + Math.round(timeDiff / 60)}) 
        if (sendNotification) {
            slack.notify({'channel': '#dev', 'text': server + ': no events for 15 minutes, lastest minutes back: ' + Math.round(timeDiff / 60)}, function(res) {
                // process.exit(hits[0]['_source']);
                process.exit(1);    
            });
        } else {
                process.exit(1);    
        }

    } else {
        process.exit();
    }
}, function (err) {
    console.log({'channel': '#dev', 'text': server + ': elastic search error'})
    slack.notify({'channel': '#dev', 'text': server + ': elastic search error'});
});

