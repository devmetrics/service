#!/bin/bash
node check_service.js "app-token-123128*" "false" "service.devmetrics.io"
status=$?
if [ $status -ne 0 ]; then 
	echo "Notification sent"
	sudo pkill -9 -u "logstash"
	sudo service logstash restart
fi
