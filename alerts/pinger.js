var http = require('http');


var event_page_open = {
  host: 'www.devmetrics.io',
  path: '/api/event?app_id=pinger2&event_name=page_opened&user_id=-1'
};

var event_btn_click = {
  host: 'www.devmetrics.io',
  path: '/api/event?app_id=pinger2&event_name=call2act_click&user_id=-1'
};

callback = function(response) {
  var str = '';

  //another chunk of data has been recieved, so append it to `str`
  response.on('data', function (chunk) {
    str += chunk;
  });

  //the whole response has been recieved, so we just print it out here
  response.on('end', function () {
    console.log(str);
  });
}


for (var i = 0; i < 30; ++i) {
  if (Math.random() < 0.7) {
    http.request(event_page_open, callback).end();
  }

  if (Math.random() < 0.45) {
    http.request(event_btn_click, callback).end();
  }
}
