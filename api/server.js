// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');
var bodyParser = require('body-parser');
var app        = express();
var morgan     = require('morgan');
var http = require('http');
var config = require('./config.js')

// configure app
app.use(morgan('dev')); // log requests to the console

// configure body parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port     = process.env.PORT || config.port || 5000; // set our port

//var mongoose   = require('mongoose');
//mongoose.connect('mongodb://node:node@novus.modulusmongo.net:27017/Iganiq8o'); // connect to our database
//var Bear     = require('./app/models/bear');

// ROUTES FOR OUR API
// =============================================================================

// create our router
var router = express.Router();

// middleware to use for all requests
router.use(function(req, res, next) {
	// do logging
	console.log('Something is happening.');
	next();
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
	res.json({ message: 'hooray! welcome to our api!' });	
});

// on routes that end in /bears
// ----------------------------------------------------
router.route('/apps')

	// create a bear (accessed at POST http://localhost:8080/bears)
	.post(function(req, res) {
		
		var bear = new Bear();		// create a new instance of the Bear model
		bear.name = req.body.name;  // set the bears name (comes from the request)

		bear.save(function(err) {
			if (err)
				res.send(err);

			res.json({ message: 'Bear created!' });
		});

		
	})

	// get all the bears (accessed at GET http://localhost:8080/api/bears)
	.get(function(req, res) {

	
     http.get("http://ehost:9200/app-token-123128*/_stats/docs", function(call_res) {
       
       console.log("Got response: " + call_res.statusCode);
       call_res.on('data', function(data){
         var jsonObject = JSON.parse(data);
         console.log("Got data: " + data);
         var jsonObject = JSON.parse(data);

         res.json(jsonObject._all.primaries.docs.count);
       });
      
     }).on('error', function(e) {
       console.log("Got error: " + e.message);
       res.send(e); 
     });

	});


    function getDocCount(appid, cb) {
	
	    local = {};
	    local.count =0;  
	
	    http.get({
	        host: config.elastichost,
	        port: config.elasticport,
	        path:  '/app-'+ appid + '*/_search?search_type=count'
	    }, function(res) {
	        // explicitly treat incoming data as utf8 (avoids issues with multi-byte chars)
	        res.setEncoding('utf8');
	 
	        // incrementally capture the incoming response body
	        var body = '';
	        res.on('data', function(d) {
	            body += d;
	        });
	 
	        // do whatever we want with the response once it's done
	        res.on('end', function() {
	            try {
	                var parsed = JSON.parse(body);
	                local.count = parsed.hits.total;
	            } catch (err) {
	                console.error('Unable to parse response as JSON', err);
	                return cb(err);
	            }
	 
	            // pass the relevant data back to the callback
	            cb(null, local.count);
	        });
	    }).on('error', function(err) {
	        // handle errors with the request itself
	        console.error('Error with the request:', err.message);
	        cb(err);
	    })

   }

    function createDashboard(appid, cb) {

   	     var spawn = require('child_process').spawn,
	     ls    = spawn('bash', ['./app_deploy_api.sh', appid ],{cwd : __dirname});

		  var body = '';
		  var errbody = '';
	
	     ls.stdout.on('data', function (data) {
	  //      console.log('stdout: ' + data);
	        body += data;
	 //       cb(data);
	     });

	     ls.stderr.on('data', function (data) {
	 //       console.log('stderr: ' + data);
	        body += data;
	 //       cb(data);
	     });

	     ls.on('close', function (code) {
  			if (code !== 0) {
    			console.error('process exited with code ' + code);
    			cb("Error external process",body)
  			}
  			cb (null, body);
	     });
	}
   
    function getDashboardInfo(appid, cb) {
	
	    local = {};
	    local.count =0;  
	
	    http.get({
	        host: config.elastichost,
	        port: config.elasticport,
	        path:  '/.kibana-'+ appid + '*/_search?search_type=count'
	    }, function(res) {
	        // explicitly treat incoming data as utf8 (avoids issues with multi-byte chars)
	        res.setEncoding('utf8');
	 
	        // incrementally capture the incoming response body
	        var body = '';
	        res.on('data', function(d) {
	            body += d;
	        });
	 
	        // do whatever we want with the response once it's done
	        res.on('end', function() {
	            try {
	                var parsed = JSON.parse(body);
	                console.log (parsed); 
	              //  local.count = parsed.took;
	                
	            } catch (err) {
	                console.error('Unable to parse response as JSON', err);
	                return cb(err);
	            }
	 
	            // pass the relevant data back to the callback
	            cb(null, {
	                count:local.count
	            });
	        });
	    }).on('error', function(err) {
	        // handle errors with the request itself
	        console.error('Error with the request:', err.message);
	        cb(err);
	    })

   }


// on routes that end in /bears/:bear_id
// ----------------------------------------------------
router.route('/apps/:app_id')



	// get the bear with that id
	.get(function(req, res) {

		getDocCount(req.params.app_id, function(err, data){

			if (err) res.send (err);
		    res.send({count:data});

		});		

  	})

	// update the bear with this id
	.put(function(req, res) {

		getDocCount(req.params.app_id, function(err, data){

			if (err) res.send (err);
			
			//console.log(data);
			
			if (data <= 0){ 
				//HTTP status - 409 Conflict
				res.status(409).send({message:"Error: No data for application, push data before Dashboard provisioning."});
			} else{
			
			createDashboard(req.params.app_id, function(err, data){
               
				if (err) {
				    console.error("Error: " + data);
					res.status(500).send ({"message":"Server Error"});
				}
				res.send({"message":"Dashboards is created successfully"});
				//res.send({"message":data})

			})
			
		   }
		})

	})

	// delete the bear with this id
	.delete(function(req, res) {
		Bear.remove({
			_id: req.params.bear_id
		}, function(err, bear) {
			if (err)
				res.send(err);

			res.json({ message: 'Successfully deleted' });
		});
	});


// REGISTER OUR ROUTES -------------------------------
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);
