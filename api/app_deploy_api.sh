#!/bin/bash

# Todo create index by send logstash packet.
#??????

app_id=$1
echo "Start provisioning for"  $app_id

cd ../dashboards/apps/pkg-20150709

bash es_deploy.sh "localhost:9200" ".kibana-"$app_id "app-"$app_id"*" "\/apps\/"$app_id
status=$?
if [ $status -ne 0 ]; then 
	exit 1
fi

echo "Components is installed"
exit 0
