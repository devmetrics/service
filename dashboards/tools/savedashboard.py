#!/usr/bin/python
import sys
import os
import json
import re
import json
import urllib2
import base64

class DashbordPackage:
	def __init__(self, server, index, dashboards):
		self.server = server
		self.index = index
		self.dashboards = dashboards
		self.data = {}
		self.data['dashboard'] = {}	
		self.data['visualization'] = {}	
		self.data['search'] = {}	
		self.data['index-pattern'] = {}	


	def post(self, uri, payload):
		url = 'http://' + server +'/' + index  + uri
		req = urllib2.Request(url)
		base64string = base64.encodestring('%s:%s' % ('amazon', '123Secret123')).replace('\n', '')
		req.add_header("Authorization", "Basic %s" % base64string)   
		req.add_header('Content-Type', 'application/json')
		r = urllib2.urlopen(req, json.dumps(payload))
		return r

  	def loadComponent(self, componentName):
  		uri = '/_search?pretty' 
		data = {"query":{"query_string":{"query":"_id:"+ componentName,"analyze_wildcard":True}},
  				"size":100, "fields":["_source"]}
  		r=self.post(uri, data)
  		jobj = json.loads(r.read())
  		res = {}	
  		try:
  			res = jobj['hits']['hits'][0]['_source'] 
  		except:
  			print "Error ", componentName
  		return res

  	

	def loadDashboards(self):
		dashboardNames = self.dashboards.split(',')
		for dashboardName in dashboardNames:
			self.data['dashboard'][dashboardName] = self.loadComponent(dashboardName)
	 
	def addIndexPattern(self, jsonComponent):
		try:
			indexPatternName=json.loads(jsonComponent['kibanaSavedObjectMeta']['searchSourceJSON'])['index']
			self.data['index-pattern'][indexPatternName] = {}
		except:
			pass

  	def loadComponents(self):
  		for dash in self.data['dashboard']:
	  		st = self.data['dashboard'][dash]['panelsJSON']
	  		jdash = json.loads(st)
	  		for comp in jdash:
	  			jobj = self.loadComponent(comp['id'])
	  			self.addIndexPattern(jobj)
	  			if comp['type'] == 'visualization':
	  				self.data['visualization'][comp['id']] = jobj
	  			else:
	  				self.data['search'][comp['id']] = jobj
	  	for comp in self.data['index-pattern']:
	  		self.data['index-pattern'][comp] = self.loadComponent(comp)


	def correctDashboard(self, jsonDashboard):
  		try:
  			jsonDashboard['title'] = jsonDashboard['title'].replace(self.postfix, "")
  			otemp = jsonDashboard['panelsJSON']
  			jtemp = json.loads (otemp)
  			# jtemp.sort(key=lambda place: jtemp.row)
  			newlist = sorted(jtemp, key= lambda k: (k['row'], k['col']))
  			for item in newlist:
  				item['id'] = self.correctComponentName(item['id'])
  			jsonDashboard['panelsJSON'] = json.dumps(newlist, sort_keys=True).lower()
  		except:
  			raise
  		return jsonDashboard

  	def correctComponentName(self, componentName):
		# componentName = componentName.replace(self.postfix, "")
		componentName = componentName.lower()
		componentName = re.sub("[()% ]", "", componentName)
		return componentName

	def correctComponent(self, jsonComponent):
  		try:
  			jsonComponent['title'] = jsonComponent['title'].replace(self.postfix, "")
  			jstr = json.dumps (jsonComponent, sort_keys=True) 
  			jsonComponent = jstr.replace(self.location, "$$location$$")
  			jsonComponent = jstr.replace(self.pattern, "$$pattern$$")
  		except:
  			raise
  		return jsonComponent

	def saveComponents(self, componentType):
		dir = './tmp/'+ componentType
		try:
			os.makedirs(dir)
		except:
			pass
		for comp in self.data[componentType]:
			jcomp = self.correctComponent(self.data[componentType][comp])
			comp = self.correctComponentName(comp)
			print comp
			with open(dir + '/' + comp + '.json', 'w') as f:
  				f.write(jcomp)
 	
	def saveDashboard(self):
		componentType = 'dashboard'
		dir = './tmp/'+ componentType
		try:
			os.makedirs(dir)
		except:
			pass
		for comp in self.data[componentType]:
			jdash = self.correctDashboard(self.data[componentType][comp])
			comp = self.correctComponentName(comp)
			with open(dir + '/' + comp + '.json', 'w') as f:
  				json.dump(jdash, f, ensure_ascii=False)
		
	def saveIndexPattern(self):
		componentType = 'index-pattern'
		dir = './tmp/'+ componentType
		try:
			os.makedirs(dir)
		except:
			pass
		for comp in self.data[componentType]:
			print comp
			jcomp = self.correctComponent(self.data[componentType][comp])
			with open(dir + '/' + componentType + '.json', 'w') as f:
  				f.write(jcomp)
 	

  	
  	def saveToFolder(self):
  		# dir = './tmp/'
		# try:
		# 	os.makedirs(dir)
		# except:
		# 	pass
		# with open(dir + '/' + 'package' + '.json', 'w') as f:
		# 	json.dump(self.data, f, ensure_ascii=False, sort_keys=True, indent=4)
	
		self.saveDashboard()
		self.saveComponents('visualization')
		self.saveComponents('search')
		self.saveIndexPattern()

		
    
if __name__ == '__main__':
    	
	server = {}
	index = {}
	dashboards = {}	
	try :
		server = sys.argv[1]
		index = sys.argv[2]
		dashboards = sys.argv[3]
		pattern = sys.argv[4]
		location = sys.argv[5]
		postfix = sys.argv[6]

	except:
		print 'script has 5 parameters: server="localhost:9200", index =".kibana", dashboards="summary, failures",', 
		' pattern="app-token-123128*",  location="/apps/token-123128", postfix=" - AZv2"'

	try:
	 	pkg = DashbordPackage(server, index, dashboards)
	 	pkg.pattern = pattern
	 	pkg.location = location
	 	pkg.postfix = postfix
	 	pkg.loadDashboards()
	 	pkg.loadComponents()
	 	pkg.saveToFolder()

	except:
		raise




