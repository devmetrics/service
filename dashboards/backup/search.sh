#!bin/bash
suffix=$(date +%Y%m%d%H%M%S)

curl -XPOST http://ec2-54-152-18-234.compute-1.amazonaws.com:9200/.kibana/_search?pretty -d '{
"query":{"query_string":{"query":"_id:VN_6-Summary","analyze_wildcard":true}},
"size":100,
"fields":["_source"]
}' >> vn_6-summary.$suffix.json


curl -XPOST http://ec2-54-152-18-234.compute-1.amazonaws.com:9200/.kibana/_search?pretty -d '{
"query":{"query_string":{"query":"_id:VN_6-Failures","analyze_wildcard":true}},
"size":100,
"fields":["_source"]
}' >> vn_6-failures.$suffix.json


curl -XPOST http://ec2-54-152-18-234.compute-1.amazonaws.com:9200/.kibana/_search?pretty -d '{
"query":{"query_string":{"query":"_id:VN_7-Summary","analyze_wildcard":true}},
"size":100,
"fields":["_source"]
}' >> vn_7-summary.$suffix.json


curl -XPOST http://ec2-54-152-18-234.compute-1.amazonaws.com:9200/.kibana/_search?pretty -d '{
"query":{"query_string":{"query":"_id:VN_7-Failures","analyze_wildcard":true}},
"size":100,
"fields":["_source"]
}' >> vn_7-failures.$suffix.json
