#!/bin/bash
if [ $# -lt 4 ] 
	then 
 		echo 'script has 4 parameters: server="localhost:9200", index =".kibana", pattern="logstash-*", location="/kibana'
 		exit 1
fi

server=$1
index=$2
pattern=$3
location=$4



curl -XPUT $server/$index"/index-pattern/"$pattern -d '


{
	"title":"'$pattern'","timeFieldName":"@timestamp","customFormats":"{}","fields":"[{\"type\":\"string\",\"indexed\":true,\"analyzed\":false,\"doc_values\":false,\"name\":\"level.raw\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":true,\"analyzed\":false,\"doc_values\":false,\"name\":\"hostip.raw\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":false,\"analyzed\":false,\"name\":\"_source\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":true,\"analyzed\":false,\"doc_values\":false,\"name\":\"event_type\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":true,\"analyzed\":true,\"doc_values\":false,\"name\":\"type\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":true,\"analyzed\":false,\"doc_values\":false,\"name\":\"@version\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":true,\"analyzed\":false,\"doc_values\":false,\"name\":\"tags.raw\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":true,\"analyzed\":false,\"doc_values\":false,\"name\":\"version\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":true,\"analyzed\":false,\"name\":\"_type\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":true,\"analyzed\":true,\"doc_values\":false,\"name\":\"level\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":false,\"analyzed\":false,\"name\":\"_id\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":true,\"analyzed\":false,\"doc_values\":false,\"name\":\"domain\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":true,\"analyzed\":true,\"doc_values\":false,\"name\":\"tags\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":true,\"analyzed\":false,\"doc_values\":false,\"name\":\"label.raw\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":true,\"analyzed\":false,\"doc_values\":false,\"name\":\"status_code\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":true,\"analyzed\":false,\"doc_values\":false,\"name\":\"host\",\"count\":0,\"scripted\":false},{\"type\":\"number\",\"indexed\":true,\"analyzed\":false,\"doc_values\":false,\"name\":\"error\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":false,\"analyzed\":false,\"name\":\"_index\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":true,\"analyzed\":false,\"doc_values\":false,\"name\":\"return\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":true,\"analyzed\":true,\"doc_values\":false,\"name\":\"label\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":true,\"analyzed\":false,\"doc_values\":false,\"name\":\"correlation\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":true,\"analyzed\":false,\"doc_values\":false,\"name\":\"severity\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":true,\"analyzed\":false,\"doc_values\":false,\"name\":\"type.raw\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":true,\"analyzed\":false,\"doc_values\":false,\"name\":\"request_uri\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":true,\"analyzed\":false,\"doc_values\":false,\"name\":\"uri\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":true,\"analyzed\":true,\"doc_values\":false,\"name\":\"message\",\"count\":0,\"scripted\":false},{\"type\":\"date\",\"indexed\":true,\"analyzed\":false,\"doc_values\":false,\"name\":\"@timestamp\",\"count\":0,\"scripted\":false},{\"type\":\"number\",\"indexed\":true,\"analyzed\":false,\"doc_values\":false,\"name\":\"response_time\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":true,\"analyzed\":false,\"doc_values\":false,\"name\":\"session\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":true,\"analyzed\":false,\"doc_values\":false,\"name\":\"method\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":true,\"analyzed\":false,\"doc_values\":false,\"name\":\"app_id\",\"count\":0,\"scripted\":false},{\"type\":\"string\",\"indexed\":true,\"analyzed\":true,\"doc_values\":false,\"name\":\"hostip\",\"count\":0,\"scripted\":false}]"
}
'
echo $LINENO

curl -XPOST $server/$index"/config/4.0.2/_update" -d '
{
	doc:{buildNum: 6004, defaultIndex: "'$pattern'"}
}'

echo $LINENO


curl -XPUT $server/$index"/visualization/VN_10-Average-Responce-Time" -d '
{
      "title":"HTTP Response Time","visState":"{\n  \"type\": \"metric\",\n  \"params\": {\n    \"fontSize\": 24\n  },\n  \"aggs\": [\n    {\n      \"id\": \"1\",\n      \"type\": \"percentiles\",\n      \"schema\": \"metric\",\n      \"params\": {\n        \"field\": \"response_time\",\n        \"percents\": [\n          95\n        ]\n      }\n    }\n  ],\n  \"listeners\": {}\n}","description":"","version":1,"kibanaSavedObjectMeta":{"searchSourceJSON":"{\n  \"index\": \"'$pattern'\",\n  \"query\": {\n    \"query_string\": {\n      \"query\": \"*\",\n      \"analyze_wildcard\": true\n    }\n  },\n  \"filter\": []\n}"}
    }'

echo $LINENO

curl -XPUT $server/$index"/visualization/VN_10-Host-Load" -d '

{"title":"Servers","visState":"{\n  \"type\": \"table\",\n  \"params\": {\n    \"perPage\": 10,\n    \"showMeticsAtAllLevels\": false,\n    \"showPartialRows\": false\n  },\n  \"aggs\": [\n    {\n      \"id\": \"1\",\n      \"type\": \"count\",\n      \"schema\": \"metric\",\n      \"params\": {}\n    },\n    {\n      \"id\": \"2\",\n      \"type\": \"terms\",\n      \"schema\": \"bucket\",\n      \"params\": {\n        \"field\": \"host\",\n        \"size\": 5,\n        \"order\": \"desc\",\n        \"orderBy\": \"1\"\n      }\n    },\n    {\n      \"id\": \"3\",\n      \"type\": \"avg\",\n      \"schema\": \"metric\",\n      \"params\": {\n        \"field\": \"response_time\"\n      }\n    }\n  ],\n  \"listeners\": {}\n}","description":"","version":1,"kibanaSavedObjectMeta":{"searchSourceJSON":"{\n  \"index\": \"'$pattern'\",\n  \"query\": {\n    \"query_string\": {\n      \"query\": \"event_type:http_request\",\n      \"analyze_wildcard\": true\n    }\n  },\n  \"filter\": []\n}"}}
'

echo $LINENO

curl -XPUT $server/$index"/visualization/VN_10-HTTP-Errors" -d '

{"title":"HTTP Errors","visState":"{\"type\":\"histogram\",\"params\":{\"shareYAxis\":true,\"addTooltip\":true,\"addLegend\":true,\"mode\":\"stacked\",\"defaultYExtents\":false},\"aggs\":[{\"id\":\"1\",\"type\":\"count\",\"schema\":\"metric\",\"params\":{}},{\"id\":\"2\",\"type\":\"date_histogram\",\"schema\":\"segment\",\"params\":{\"field\":\"@timestamp\",\"interval\":\"auto\",\"min_doc_count\":1,\"extended_bounds\":{}}},{\"id\":\"3\",\"type\":\"terms\",\"schema\":\"group\",\"params\":{\"field\":\"status_code\",\"include\":{\"pattern\":\"\"},\"size\":10,\"order\":\"desc\",\"orderBy\":\"1\"}}],\"listeners\":{}}","description":"","version":1,"kibanaSavedObjectMeta":{"searchSourceJSON":"{\"index\":\"'$pattern'\",\"query\":{\"query_string\":{\"query\":\"event_type:http_request AND status_code:/[4,5][0-9][0-9]/\",\"analyze_wildcard\":true}},\"filter\":[]}"}}
'

echo $LINENO

curl -XPUT $server/$index"/visualization/VN_10-HTTP-Response-Time-histogram" -d'
{"title":"HTTP Response Time histogram","visState":"{\"type\":\"histogram\",\"params\":{\"addLegend\":false,\"addTooltip\":true,\"defaultYExtents\":true,\"mode\":\"grouped\",\"shareYAxis\":true,\"spyPerPage\":10},\"aggs\":[{\"id\":\"1\",\"type\":\"count\",\"schema\":\"metric\",\"params\":{}},{\"id\":\"2\",\"type\":\"range\",\"schema\":\"group\",\"params\":{\"field\":\"response_time\",\"ranges\":[{\"from\":0,\"to\":500},{\"from\":500,\"to\":1500},{\"from\":1500,\"to\":4500},{\"from\":4500,\"to\":null}]}}],\"listeners\":{}}","description":"","version":1,"kibanaSavedObjectMeta":{"searchSourceJSON":"{\"index\":\"'$pattern'\",\"query\":{\"query_string\":{\"query\":\"event_type:http_request\",\"analyze_wildcard\":true}},\"filter\":[]}"}}'

echo $LINENO

curl -XPUT $server/$index"/visualization/VN_10-HTTP-Server-Throughput" -d'
{"title":"HTTP Server Throughput","visState":"{\"type\":\"line\",\"params\":{\"shareYAxis\":true,\"addTooltip\":true,\"addLegend\":true,\"defaultYExtents\":false},\"aggs\":[{\"id\":\"1\",\"type\":\"count\",\"schema\":\"metric\",\"params\":{}},{\"id\":\"2\",\"type\":\"date_histogram\",\"schema\":\"segment\",\"params\":{\"field\":\"@timestamp\",\"interval\":\"auto\",\"min_doc_count\":1,\"extended_bounds\":{}}}],\"listeners\":{}}","description":"","version":1,"kibanaSavedObjectMeta":{"searchSourceJSON":"{\"index\":\"'$pattern'\",\"query\":{\"query_string\":{\"query\":\"event_type:http_request\",\"analyze_wildcard\":true}},\"filter\":[]}"}}

'
echo $LINENO

curl -XPUT $server/$index"/visualization/VN_10-Server-Response-Time" -d'
{"title":"HTTP Response Time","visState":"{\n  \"type\": \"line\",\n  \"params\": {\n    \"shareYAxis\": true,\n    \"addTooltip\": true,\n    \"addLegend\": true,\n    \"defaultYExtents\": true\n  },\n  \"aggs\": [\n    {\n      \"id\": \"1\",\n      \"type\": \"percentiles\",\n      \"schema\": \"metric\",\n      \"params\": {\n        \"field\": \"response_time\",\n        \"percents\": [\n          95\n        ]\n      }\n    },\n    {\n      \"id\": \"2\",\n      \"type\": \"date_histogram\",\n      \"schema\": \"segment\",\n      \"params\": {\n        \"field\": \"@timestamp\",\n        \"interval\": \"auto\",\n        \"min_doc_count\": 1,\n        \"extended_bounds\": {}\n      }\n    },\n    {\n      \"id\": \"3\",\n      \"type\": \"avg\",\n      \"schema\": \"metric\",\n      \"params\": {\n        \"field\": \"response_time\"\n      }\n    }\n  ],\n  \"listeners\": {}\n}","description":"","version":1,"kibanaSavedObjectMeta":{"searchSourceJSON":"{\n  \"index\": \"'$pattern'\",\n  \"query\": {\n    \"query_string\": {\n      \"query\": \"event_type:http_request\",\n      \"analyze_wildcard\": true\n    }\n  },\n  \"filter\": []\n}"}}
'

echo $LINENO

curl -XPUT $server/$index"/visualization/VN_10-Sessions-for-period" -d'
{"title":"User Sessions","visState":"{\"type\":\"metric\",\"params\":{\"fontSize\":24},\"aggs\":[{\"id\":\"1\",\"type\":\"cardinality\",\"schema\":\"metric\",\"params\":{\"field\":\"session\"}}],\"listeners\":{}}","description":"","version":1,"kibanaSavedObjectMeta":{"searchSourceJSON":"{\"index\":\"'$pattern'\",\"query\":{\"query_string\":{\"query\":\"*\",\"analyze_wildcard\":true}},\"filter\":[]}"}}
'
echo $LINENO

curl -XPUT $server/$index"/search/VN_10-User-Events" -d'
{"title":"App and Custom Events","description":"","hits":0,"columns":["message","level"],"sort":["@timestamp","desc"],"version":1,"kibanaSavedObjectMeta":{"searchSourceJSON":"{\n  \"index\": \"'$pattern'\",\n  \"query\": {\n    \"query_string\": {\n      \"query\": \"event_type:user_event\",\n      \"analyze_wildcard\": true\n    }\n  },\n  \"highlight\": {\n    \"pre_tags\": [\n      \"@kibana-highlighted-field@\"\n    ],\n    \"post_tags\": [\n      \"@/kibana-highlighted-field@\"\n    ],\n    \"fields\": {\n      \"*\": {}\n    }\n  },\n  \"filter\": []\n}"}}
'

echo $LINENO

curl -XPUT $server/$index"/visualization/VN_10-Component-Errors" -d'
{"title":"DB Engine and Sub-Services Errors","visState":"{\"type\":\"histogram\",\"params\":{\"addLegend\":true,\"addTooltip\":true,\"defaultYExtents\":false,\"mode\":\"stacked\",\"shareYAxis\":true},\"aggs\":[{\"id\":\"1\",\"type\":\"count\",\"schema\":\"metric\",\"params\":{}},{\"id\":\"2\",\"type\":\"date_histogram\",\"schema\":\"segment\",\"params\":{\"field\":\"@timestamp\",\"interval\":\"auto\",\"min_doc_count\":1,\"extended_bounds\":{}}},{\"id\":\"3\",\"type\":\"terms\",\"schema\":\"group\",\"params\":{\"field\":\"uri\",\"include\":{\"pattern\":\"\"},\"size\":10,\"order\":\"desc\",\"orderBy\":\"1\"}}],\"listeners\":{}}","description":"","version":1,"kibanaSavedObjectMeta":{"searchSourceJSON":"{\"index\":\"'$pattern'\",\"query\":{\"query_string\":{\"query\":\"!event_type:http_request AND error:1\",\"analyze_wildcard\":true}},\"filter\":[]}"}}
'

echo $LINENO

curl -XPUT $server/$index"/visualization/VN_10-Component-Throughput" -d'
{"title":"DB Engine and Sub-Services Load","visState":"{\"type\":\"histogram\",\"params\":{\"addLegend\":true,\"addTooltip\":true,\"defaultYExtents\":false,\"mode\":\"stacked\",\"shareYAxis\":true},\"aggs\":[{\"id\":\"1\",\"type\":\"count\",\"schema\":\"metric\",\"params\":{}},{\"id\":\"2\",\"type\":\"date_histogram\",\"schema\":\"segment\",\"params\":{\"field\":\"@timestamp\",\"interval\":\"auto\",\"min_doc_count\":1,\"extended_bounds\":{}}},{\"id\":\"3\",\"type\":\"terms\",\"schema\":\"group\",\"params\":{\"field\":\"uri\",\"include\":{\"pattern\":\"\"},\"size\":10,\"order\":\"desc\",\"orderBy\":\"1\"}}],\"listeners\":{}}","description":"","version":1,"kibanaSavedObjectMeta":{"searchSourceJSON":"{\"index\":\"'$pattern'\",\"query\":{\"query_string\":{\"query\":\"!event_type:http_request\",\"analyze_wildcard\":true}},\"filter\":[]}"}}

'

echo $LINENO

curl -XPUT $server/$index"/visualization/VN_10-Components-consumption" -d'
{"title":"DB Engine and Sub-Services Consumption(throughput * response_time)","visState":"{\n  \"type\": \"histogram\",\n  \"params\": {\n    \"addLegend\": true,\n    \"addTooltip\": true,\n    \"defaultYExtents\": true,\n    \"mode\": \"stacked\",\n    \"shareYAxis\": true\n  },\n  \"aggs\": [\n    {\n      \"id\": \"1\",\n      \"type\": \"sum\",\n      \"schema\": \"metric\",\n      \"params\": {\n        \"field\": \"response_time\"\n      }\n    },\n    {\n      \"id\": \"2\",\n      \"type\": \"date_histogram\",\n      \"schema\": \"segment\",\n      \"params\": {\n        \"field\": \"@timestamp\",\n        \"interval\": \"auto\",\n        \"min_doc_count\": 1,\n        \"extended_bounds\": {}\n      }\n    },\n    {\n      \"id\": \"3\",\n      \"type\": \"terms\",\n      \"schema\": \"group\",\n      \"params\": {\n        \"field\": \"uri\",\n        \"size\": 10,\n        \"order\": \"desc\",\n        \"orderBy\": \"1\"\n      }\n    }\n  ],\n  \"listeners\": {}\n}","description":"","version":1,"kibanaSavedObjectMeta":{"searchSourceJSON":"{\n  \"index\": \"'$pattern'\",\n  \"query\": {\n    \"query_string\": {\n      \"analyze_wildcard\": true,\n      \"query\": \"!event_type:http_request AND response_time:>=0\"\n    }\n  },\n  \"filter\": []\n}"}}
'

echo $LINENO

curl -XPUT $server/$index"/visualization/VN_10-Session-Time-Line" -d'
{"title":"Session Time Line","visState":"{\"type\":\"line\",\"params\":{\"addLegend\":false,\"addTooltip\":true,\"defaultYExtents\":false,\"shareYAxis\":true},\"aggs\":[{\"id\":\"1\",\"type\":\"cardinality\",\"schema\":\"metric\",\"params\":{\"field\":\"session\"}},{\"id\":\"2\",\"type\":\"date_histogram\",\"schema\":\"segment\",\"params\":{\"field\":\"@timestamp\",\"interval\":\"auto\",\"min_doc_count\":1,\"extended_bounds\":{}}}],\"listeners\":{}}","description":"","version":1,"kibanaSavedObjectMeta":{"searchSourceJSON":"{\"index\":\"'$pattern'\",\"query\":{\"query_string\":{\"analyze_wildcard\":true,\"query\":\"*\"}},\"filter\":[]}"}}
'

echo $LINENO

curl -XPUT $server/$index"/visualization/VN_10-Summary-Description" -d'{"title":"Summary Description","visState":"{\"type\":\"markdown\",\"params\":{\"markdown\":\"####**Service Health and Performance:        **                                   ![id](http://maplight.org/sites/all/modules/map/modules/map_lobby/img/details.png) ** [Performance]('$location'/#/dashboard/VN_10-Performance) ** ![id](http://maplight.org/sites/all/modules/map/modules/map_lobby/img/details.png)** [Failures]('$location'/#/dashboard/VN_10-Failures)** ![id](http://maplight.org/sites/all/modules/map/modules/map_lobby/img/details.png) ** [Log Analysis]('$location'/#/dashboard/VN_10-Log-Analysis)**\\n[](Summary dashboard shows base metric of your application. You can see details into child dashboards:)\"},\"aggs\":[],\"listeners\":{}}","description":"Proceed with caution\nModifying objects is for advanced users only. Object properties are not validated and invalid objects could cause errors, data loss, or worse. Unless someone with intimate knowledge of the code told you to be in here, you probably shouldnot be.","version":1,"kibanaSavedObjectMeta":{"searchSourceJSON":"{\"query\":{\"query_string\":{\"analyze_wildcard\":true,\"query\":\"*\"}},\"filter\":[]}"}}'

echo $LINENO

curl -XPUT $server/$index"/visualization/VN_10-HTTP-Errors-for-period" -d'
{"title":"HTTP Errors","visState":"{\"type\":\"metric\",\"params\":{\"fontSize\":24},\"aggs\":[{\"id\":\"1\",\"type\":\"count\",\"schema\":\"metric\",\"params\":{}}],\"listeners\":{}}","description":"","version":1,"kibanaSavedObjectMeta":{"searchSourceJSON":"{\"index\":\"'$pattern'\",\"query\":{\"query_string\":{\"query\":\"(event_type:http_request) AND (status_code: /[4,5][0-9][0-9]/)\",\"analyze_wildcard\":true}},\"filter\":[]}"}}
'

echo $LINENO

curl -XPUT $server/$index"/visualization/VN_10-HTTP-Status-histogram-1" -d'
{

    "title": "HTTP Status histogram",
    "visState": "{\"type\":\"histogram\",\"params\":{\"shareYAxis\":true,\"addTooltip\":true,\"addLegend\":true,\"mode\":\"grouped\",\"defaultYExtents\":true,\"spyPerPage\":10},\"aggs\":[{\"id\":\"1\",\"type\":\"count\",\"schema\":\"metric\",\"params\":{}},{\"id\":\"2\",\"type\":\"terms\",\"schema\":\"group\",\"params\":{\"field\":\"status_code\",\"exclude\":{\"pattern\":\"\"},\"size\":6,\"order\":\"desc\",\"orderBy\":\"1\"}}],\"listeners\":{}}",
    "description": "",
    "version": 1,
        "kibanaSavedObjectMeta": {
        "searchSourceJSON": "{\"index\":\"'$pattern'\",\"query\":{\"query_string\":{\"query\":\"event_type:http_request\",\"analyze_wildcard\":true}},\"filter\":[]}"
        }
}

'

echo $LINENO

curl -XPUT $server/$index"/visualization/VN_10-Navigate" -d'
{"title":"Navigation","visState":"{\"type\":\"markdown\",\"params\":{\"markdown\":\"####**Use links to navigate:** ![id](http://maplight.org/sites/all/modules/map/modules/map_lobby/img/details.png) ** [Summary]('$location'/#/dashboard/VN_10-Summary)** ![id](http://maplight.org/sites/all/modules/map/modules/map_lobby/img/details.png) ** [Performance]('$location'/#/dashboard/VN_10-Performance) ** ![id](http://maplight.org/sites/all/modules/map/modules/map_lobby/img/details.png)** [Failures]('$location'/#/dashboard/VN_10-Failures)** ![id](http://maplight.org/sites/all/modules/map/modules/map_lobby/img/details.png)** [Log Analysis]('$location'/#/dashboard/VN_10-Log-Analysis)**\\n\\n[](Summary dashboard shows base metric of your application. You can see details into child dashboards:)\"},\"aggs\":[],\"listeners\":{}}","description":"Proceed with caution\nModifying objects is for advanced users only. Object properties are not validated and invalid objects could cause errors, data loss, or worse. Unless someone with intimate knowledge of the code told you to be in here, you probably shouldnot be.","version":1,"kibanaSavedObjectMeta":{"searchSourceJSON":"{\"query\":{\"query_string\":{\"analyze_wildcard\":true,\"query\":\"*\"}},\"filter\":[]}"}}
'

echo $LINENO

curl -XPUT $server/$index"/visualization/VN_10-Top-HTTP-Failors" -d'
{"title":"Top HTTP Failors","visState":"{\"type\":\"table\",\"params\":{\"perPage\":20,\"showPartialRows\":false,\"showMeticsAtAllLevels\":false},\"aggs\":[{\"id\":\"1\",\"type\":\"count\",\"schema\":\"metric\",\"params\":{}},{\"id\":\"3\",\"type\":\"terms\",\"schema\":\"bucket\",\"params\":{\"field\":\"request_uri\",\"size\":20,\"order\":\"desc\",\"orderBy\":\"1\"}}],\"listeners\":{}}","description":"","version":1,"kibanaSavedObjectMeta":{"searchSourceJSON":"{\"index\":\"'$pattern'\",\"query\":{\"query_string\":{\"query\":\"event_type:http_request AND status_code:[400 TO * ]\",\"analyze_wildcard\":true}},\"filter\":[]}"}}
'

echo $LINENO

curl -XPUT $server/$index"/visualization/VN_10-Components-Description" -d'

{
    "title": "DB engine and Sub-services",
    "visState": "{\"type\":\"markdown\",\"params\":{\"markdown\":\"#### **Validate work of DB engine and  Sub-services**.\"},\"aggs\":[],\"listeners\":{}}",
    "description": "Proceed with caution\nModifying objects is for advanced users only. Object properties are not validated and invalid objects could cause errors, data loss, or worse. Unless someone with intimate knowledge of the code told you to be in here, you probably shouldnt be.",
    "version": 1,
    "kibanaSavedObjectMeta": {
      "searchSourceJSON": "{\"query\":{\"query_string\":{\"analyze_wildcard\":true,\"query\":\"*\"}},\"filter\":[]}"
    }
  }
'

echo $LINENO

curl -XPUT $server/$index"/visualization/VN_10-Components-Performance-Description" -d'

{
    "title": "DB engine and Sub-services performance",
    "visState": "{\"type\":\"markdown\",\"params\":{\"markdown\":\"#### **Validate work of DB engine and  Sub-services, check slowest calls and add filters to get related info in the log**.\"},\"aggs\":[],\"listeners\":{}}",
    "description": "Proceed with caution\nModifying objects is for advanced users only. Object properties are not validated and invalid objects could cause errors, data loss, or worse. Unless someone with intimate knowledge of the code told you to be in here, you probably shouldnt be.",
    "version": 1,
    "kibanaSavedObjectMeta": {
      "searchSourceJSON": "{\"query\":{\"query_string\":{\"analyze_wildcard\":true,\"query\":\"*\"}},\"filter\":[]}"
    }
  }
'

echo $LINENO

curl -XPUT $server/$index"/visualization/VN_10-Components-Failures-Description" -d'

{
    "title": "DB engine and Sub-services failures",
    "visState": "{\"type\":\"markdown\",\"params\":{\"markdown\":\"#### **Validate work of DB engine and  Sub-services, check most often failed calls and add filters to get related info in the log**.\"},\"aggs\":[],\"listeners\":{}}",
    "description": "Proceed with caution\nModifying objects is for advanced users only. Object properties are not validated and invalid objects could cause errors, data loss, or worse. Unless someone with intimate knowledge of the code told you to be in here, you probably shouldnt be.",
    "version": 1,
    "kibanaSavedObjectMeta": {
      "searchSourceJSON": "{\"query\":{\"query_string\":{\"analyze_wildcard\":true,\"query\":\"*\"}},\"filter\":[]}"
    }
  }
'


echo $LINENO

curl -XPUT $server/$index"/visualization/VN_10-Top-Components-Failors" -d'

{"title":"Top DB Engine and Sub-Services Failors","visState":"{\"type\":\"table\",\"params\":{\"perPage\":20,\"showPartialRows\":false,\"showMeticsAtAllLevels\":false},\"aggs\":[{\"id\":\"1\",\"type\":\"count\",\"schema\":\"metric\",\"params\":{}},{\"id\":\"3\",\"type\":\"terms\",\"schema\":\"bucket\",\"params\":{\"field\":\"uri\",\"size\":20,\"order\":\"desc\",\"orderBy\":\"1\"}}],\"listeners\":{}}","description":"","version":1,"kibanaSavedObjectMeta":{"searchSourceJSON":"{\"index\":\"'$pattern'\",\"query\":{\"query_string\":{\"query\":\"(!event_type:http_request) AND (error:>0)\",\"analyze_wildcard\":true}},\"filter\":[]}"}}
'

echo $LINENO

curl -XPUT $server/$index"/visualization/VN_10-Event-Log-types" -d'

{"title":"Event Log types","visState":"{\"type\":\"table\",\"params\":{\"perPage\":20,\"showPartialRows\":false,\"showMeticsAtAllLevels\":false},\"aggs\":[{\"id\":\"1\",\"type\":\"count\",\"schema\":\"metric\",\"params\":{}},{\"id\":\"2\",\"type\":\"terms\",\"schema\":\"bucket\",\"params\":{\"field\":\"event_type\",\"size\":5,\"order\":\"desc\",\"orderBy\":\"1\"}}],\"listeners\":{}}","description":"","version":1,"kibanaSavedObjectMeta":{"searchSourceJSON":"{\"index\":\"'$pattern'\",\"query\":{\"query_string\":{\"analyze_wildcard\":true,\"query\":\"*\"}},\"filter\":[]}"}}
'

echo $LINENO

curl -XPUT $server/$index"/search/VN_10-Event-Log" -d'

{"title":"Event Log","description":"","hits":0,"columns":["level","message","event_type"],"sort":["@timestamp","desc"],"version":1,"kibanaSavedObjectMeta":{"searchSourceJSON":"{\"index\":\"'$pattern'\",\"query\":{\"query_string\":{\"query\":\"*\",\"analyze_wildcard\":true}},\"highlight\":{\"pre_tags\":[\"@kibana-highlighted-field@\"],\"post_tags\":[\"@/kibana-highlighted-field@\"],\"fields\":{\"*\":{}}},\"filter\":[]}"}}
'

echo $LINENO

curl -XPUT $server/$index"/visualization/VN_10-Hosts" -d'

{"title":"Servers","visState":"{\n  \"type\": \"table\",\n  \"params\": {\n    \"perPage\": 10,\n    \"showMeticsAtAllLevels\": false,\n    \"showPartialRows\": false\n  },\n  \"aggs\": [\n    {\n      \"id\": \"1\",\n      \"type\": \"count\",\n      \"schema\": \"metric\",\n      \"params\": {}\n    },\n    {\n      \"id\": \"2\",\n      \"type\": \"terms\",\n      \"schema\": \"bucket\",\n      \"params\": {\n        \"field\": \"host\",\n        \"size\": 5,\n        \"order\": \"desc\",\n        \"orderBy\": \"1\"\n      }\n    }\n  ],\n  \"listeners\": {}\n}","description":"","version":1,"kibanaSavedObjectMeta":{"searchSourceJSON":"{\n  \"index\": \"'$pattern'\",\n  \"query\": {\n    \"query_string\": {\n      \"query\": \"*\",\n      \"analyze_wildcard\": true\n    }\n  },\n  \"filter\": []\n}"}}
'


echo $LINENO

#######

curl -XPUT $server/$index"/visualization/VN_10-Event-Severity" -d'
{
  "title": "Event Severity",
  "visState": "{\"aggs\":[{\"id\":\"1\",\"params\":{},\"schema\":\"metric\",\"type\":\"count\"},{\"id\":\"2\",\"params\":{\"exclude\":{\"pattern\":\"\\\"http\\\" | \\\"db\"},\"field\":\"level\",\"include\":{\"pattern\":\"\"},\"order\":\"desc\",\"orderBy\":\"1\",\"size\":5},\"schema\":\"segment\",\"type\":\"terms\"}],\"listeners\":{},\"params\":{\"addLegend\":true,\"addTooltip\":true,\"isDonut\":false,\"shareYAxis\":true,\"spyPerPage\":10},\"type\":\"pie\"}",
  "description": "",
  "version": 1,
  "kibanaSavedObjectMeta": {
    "searchSourceJSON": "{\"index\":\"'$pattern'\",\"query\":{\"query_string\":{\"analyze_wildcard\":true,\"query\":\"*\"}},\"filter\":[]}"
  }
}
'


echo $LINENO

curl -XPUT $server/$index"/visualization/VN_10-Log-Events-for-period" -d'

{"title":"Custom Errors","visState":"{\"type\":\"metric\",\"params\":{\"fontSize\":24},\"aggs\":[{\"id\":\"1\",\"type\":\"count\",\"schema\":\"metric\",\"params\":{}}],\"listeners\":{}}","description":"","version":1,"kibanaSavedObjectMeta":{"searchSourceJSON":"{\"index\":\"'$pattern'\",\"query\":{\"query_string\":{\"query\":\"level:error\",\"analyze_wildcard\":true}},\"filter\":[]}"}}
'
echo $LINENO

curl -XPUT $server/$index"/visualization/VN_10-Log-TimeLine" -d'

{"title":"Log Time Line","visState":"{\"aggs\":[{\"id\":\"1\",\"params\":{},\"schema\":\"metric\",\"type\":\"count\"},{\"id\":\"2\",\"params\":{\"extended_bounds\":{},\"field\":\"@timestamp\",\"interval\":\"auto\",\"min_doc_count\":1},\"schema\":\"segment\",\"type\":\"date_histogram\"},{\"id\":\"3\",\"params\":{\"field\":\"level\",\"order\":\"desc\",\"orderBy\":\"1\",\"size\":5},\"schema\":\"group\",\"type\":\"terms\"}],\"listeners\":{},\"params\":{\"addLegend\":true,\"addTooltip\":true,\"defaultYExtents\":false,\"mode\":\"stacked\",\"shareYAxis\":true,\"spyPerPage\":10},\"type\":\"histogram\"}","description":"","version":1,"kibanaSavedObjectMeta":{"searchSourceJSON":"{\"index\":\"'$pattern'\",\"query\":{\"query_string\":{\"analyze_wildcard\":true,\"query\":\"*\"}},\"filter\":[]}"}}
'
echo $LINENO

curl -XPUT $server/$index"/visualization/VN_10-Top-Slowest-Component-call" -d'

{"title":"Top Slowest DB Engine and Sub-Services call","visState":"{\"type\":\"table\",\"params\":{\"perPage\":20,\"showPartialRows\":false,\"showMeticsAtAllLevels\":false},\"aggs\":[{\"id\":\"1\",\"type\":\"count\",\"schema\":\"metric\",\"params\":{}},{\"id\":\"2\",\"type\":\"percentiles\",\"schema\":\"metric\",\"params\":{\"field\":\"response_time\",\"percents\":[95]}},{\"id\":\"3\",\"type\":\"terms\",\"schema\":\"bucket\",\"params\":{\"field\":\"uri\",\"size\":20,\"order\":\"desc\",\"orderBy\":\"2.95\"}}],\"listeners\":{}}","description":"","version":1,"kibanaSavedObjectMeta":{"searchSourceJSON":"{\"index\":\"'$pattern'\",\"query\":{\"query_string\":{\"query\":\"!event_type:http_request\",\"analyze_wildcard\":true}},\"filter\":[]}"}}
'
echo $LINENO

curl -XPUT $server/$index"/visualization/VN_10-Top-Slowest-HTTP-Requests" -d'

{"title":"Top Slowest HTTP Requests","visState":"{\"type\":\"table\",\"params\":{\"perPage\":20,\"showPartialRows\":false,\"showMeticsAtAllLevels\":false},\"aggs\":[{\"id\":\"1\",\"type\":\"count\",\"schema\":\"metric\",\"params\":{}},{\"id\":\"2\",\"type\":\"percentiles\",\"schema\":\"metric\",\"params\":{\"field\":\"response_time\",\"percents\":[95]}},{\"id\":\"3\",\"type\":\"terms\",\"schema\":\"bucket\",\"params\":{\"field\":\"request_uri\",\"size\":20,\"order\":\"desc\",\"orderBy\":\"2.95\"}}],\"listeners\":{}}","description":"","version":1,"kibanaSavedObjectMeta":{"searchSourceJSON":"{\"index\":\"'$pattern'\",\"query\":{\"query_string\":{\"query\":\"event_type:http_request\",\"analyze_wildcard\":true}},\"filter\":[]}"}}
'

echo $LINENO

curl -XPUT $server/$index"/search/VN_10-Performance-Log" -d'

{"title":"Performance Log","description":"","hits":0,"columns":["response_time","message","event_type"],"sort":["@timestamp","desc"],"version":1,"kibanaSavedObjectMeta":{"searchSourceJSON":"{\"index\":\"'$pattern'\",\"query\":{\"query_string\":{\"query\":\"*\",\"analyze_wildcard\":true}},\"highlight\":{\"pre_tags\":[\"@kibana-highlighted-field@\"],\"post_tags\":[\"@/kibana-highlighted-field@\"],\"fields\":{\"*\":{}}},\"filter\":[]}"}}
'

echo $LINENO
