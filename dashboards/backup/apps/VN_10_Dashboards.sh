#!/bin/bash
if [ $# -lt 3 ] 
	then 
 		echo 'script has 3 parameters: server="localhost:9200", index =".kibana", pattern="logstash-*"'
 		exit 1
fi

server=$1
index=$2
pattern=$3


cd dashboard-orig

for d in *; do
filename=${d%.*}
  echo $filename
  curl -XPUT $server/$index"/dashboard/"$filename -d@$d
done

cd ..

