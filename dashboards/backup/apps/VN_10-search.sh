if [ $# -lt 3 ] 
	then 
 		echo 'script has 3 parameters: server="localhost:9200", index =".kibana", pattern="logstash-*"'
 		exit 1
fi

server=$1
index=$2
pattern=$3


#!bin/bash
preffix="./temp/"$(date +%Y-%m-%d-%H%M%S)
suffix=""

mkdir $preffix

## declare an array variable
declare -a arr=(
"VN_7-Summary" 
"VN_7-Failures"
"VN_10-Performance"
"VN_10-Log-Analysis"

"VN_5-Sessions-for-period"
"VN_6-Session-Time-Line"
"VN_5-Average-Responce-Time"
"VN_5-HTTP-Response-Time-histogram"
"VN_8-HTTP-Errors-for-period"
"VN_7_1-Summary-Description"
"VN_5-Server-Response-Time"
"VN_5-HTTP-Server-Throughput"
"VN_5-HTTP-Errors"
"VN_5_1-Components-consumption"
"VN_5_1-Component-Throughput"
"VN_5_1-Component-Errors"
"VN_5-Host-Load"
"VN_5-User-Events"

"VN_5-HTTP-Status-histogram-1"
"VN_7_1-Navigate"
"VN_5-Top-HTTP-Failors"
"VN_7-How-to-diagnostic-failure-spike"
"VN_5_1-Top-Components-Failors"
"VN_5-Event-Log-types"
"VN_5-Event-Log"
"VN_5-Hosts"

"VN_5-Top-Slowest-HTTP-Requests"
"VN_5_1-Top-Slowest-Component-call"
"VN_10-Performance-Log"
"VN_10-Log-Events-for-period"
"VN_10-Event-Severity"
"VN_5-Log-TimeLine"

	)


## now loop through the above array
for id in "${arr[@]}"
do

   echo "$id"
   
curl -XPOST $server/$index/_search?pretty -d '{
  "query":{"query_string":{"query":"_id:'$id'","analyze_wildcard":true}},
  "size":100,
  "fields":["_source"]
  }' >> $preffix/$id$suffix.json

   # or do whatever with individual element of the array
done

patternfile=${pattern/-*/}  

echo $pattern

curl -XPOST $server/$index/_search -d '{
  "query":{"query_string":{"query":"_id:'$pattern'","analyze_wildcard":true}},
  "size":100,
  "fields":["_source"]
  }' >> $preffix/$patternfile$suffix.json








