#!/bin/bash

SERVERNAME=it.devmetrics.io

#Update Sources configuration

git pull
echo
echo "Sources has been updated from git:https://vladimir_devmetrics@bitbucket.org/devmetrics/service.git"
echo " if there are no repository, clone it: git clone https://vladimir_devmetrics@bitbucket.org/devmetrics/service.git "

#Update Logstash configuration
sudo cp -v ./logstash/$SERVERNAME/logstash.apps.conf /etc/logstash/conf.d/
sudo cp -v ./logstash/template.apps.json /etc/logstash/template.apps.json

sudo cp -v ./logstash/$SERVERNAME/logstash.collectd.conf /etc/logstash/conf.d/
sudo cp -v ./logstash/template.collectd.json /etc/logstash/template.collectd.json

sudo cp -v ./logstash/$SERVERNAME/logstash.influxdb.conf /etc/logstash/conf.d/

echo
echo "Logstash configuration has been copied if need restart: sudo service logstash restart"
echo
#Update NGINX configuration
sudo cp -v ./config/nginx/$SERVERNAME /etc/nginx/sites-available/
sudo ln -s /etc/nginx/sites-available/$SERVERNAME /etc/nginx/sites-enabled/
echo
echo "NGINX configuration has been copied if need restart: sudo service nginx restart"
echo

cd api
pm2 start server.js
pm2 save

echo "Check that service is working"
curl -XGET localhost/api/apps/token-1/

cd ..

# Elastic Search API
sudo cp -v ./config/nginx/elasticsearch /etc/nginx/sites-available/
sudo ln -s /etc/nginx/sites-available/elasticsearch /etc/nginx/sites-enabled/
echo
echo "Elastic Serach endpoint has been added to NGINX configuration if need restart: sudo service nginx restart"
echo

echo "Update cron tasks"
cp -v ./cron/$SERVERNAME/* ./cron/
bash ./cron/create_cron_task.sh

#experiments with collectd

sudo cp -v ./collectd/$SERVERNAME/collectd.conf /etc/collectd/

echo
echo "Collectd configuration has been copied if need restart: sudo service collectd restart"
echo




