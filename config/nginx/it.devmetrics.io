server {
    listen 80;

    #server_name example.com;
    server_name 52.7.58.197;

#    auth_basic "Restricted Access";
#    auth_basic_user_file /etc/nginx/htpasswd.users;

#    error_log   /var/log/nginx/error-kb4.log notice;
#    rewrite_log on;


    location / {
        root /home/ubuntu/projects/service/www;
    }

    location /api {
        proxy_pass http://localhost:5540/api;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }


   location ~* ^/apps/.* {
        rewrite ^/apps/([^/]*)/(.*) /$2 break;
        proxy_pass http://127.0.0.1:5601;
            proxy_set_header Host $host;
            proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header   Kibana-Index .kibana-$1;
        proxy_send_timeout 600s;
        proxy_read_timeout 600s;
    
    auth_basic "Restricted Access";
    auth_basic_user_file /etc/nginx/htpasswd.users;

  }
}