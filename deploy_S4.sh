#!/bin/bash

SERVERNAME=demo_load-s4

#Update Sources configuration

git pull

#tsung configuration
cp -v ./tsung/dohtml.sh /home/ubuntu/tsung/dohtml.sh
cp -v ./tsung/site_load_heroku.xml /home/ubuntu/tsung/site_load_heroku.xml

echo "Update cron tasks"
cp -v ./cron/$SERVERNAME/* ./cron/
bash ./cron/create_cron_task.sh 


#experiments with collectd

sudo cp -v ./collectd/$SERVERNAME/collectd.conf /etc/collectd/

echo
echo "Collectd configuration has been copied if need restart: sudo service collectd restart"
echo

