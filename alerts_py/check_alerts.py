import sys
import slackpy
import json
from datetime import datetime
from datetime import timedelta

from elasticsearch import Elasticsearch

# cpuUsageAlert={}
# cpuUsageAlert['index'] = "collectd-*"
# cpuUsageAlert['body'] = {
#   "query": { "filtered": 
# 	{ 
# 		"query": {
#         	"query_string": {
#         	  	"query": "plugin: curl_json AND type_instance: \"cpu-usage\" AND value:>0 ",
#           		"analyze_wildcard": True
#         	}
#       	},
#       	"filter": {"range": {"@timestamp": {"gte": "now-5m" }}}
#     }
#   }
# }
# cpuUsageAlert['message'] = "%(@timestamp)s Attention!!! High Load, cpu Usage higher 80% : %(host)s %(value)s" 

def needSend(key):
	sendlist = {}
	currtime = datetime.utcnow()
	lasttime = currtime
	fl = None
	result = True 

	try:
		fl = open (config['config']['throttling_file'] , "r+")
		sendlist = json.load(fl)
		lasttime = datetime.strptime(sendlist[key],"%Y-%m-%dT%H:%M:%S.%fZ")
		result = False
	except Exception, e:
		pass
	finally:
		if fl is not None:
			fl.close()

	if ((result == True) or ((currtime - lasttime ) > timedelta(minutes=15))):
		result = True 
		fl = None
		try:
			sendlist[key] = datetime.strftime(currtime,"%Y-%m-%dT%H:%M:%S.%fZ")
			fl = open (config['config']['throttling_file'], "w+")
			json.dump(sendlist, fl)	
		except Exception, e:
			log ("Cannot write log file %s" % e)
			pass
		finally:
			if fl is not None:
				fl.close()
	return result




def log(sMessage):
	global config
	global logging
	slack_on = config['config']['slack_on']
	if (config is not None) and (slack_on == True):
		# LogLevel: INFO
		if logging is None:
			logging = slackpy.SlackLogger(config['slack']['incoming_web_hook'],
				config['slack']['channel'],
				config['slack']['user_name'])
		logging.info(message=sMessage)
	print (sMessage)

def check_alert(condition):
	res = es.search(index=condition["index"], body=condition["body"])
	for hit in res['hits']['hits']:
		if (needSend(condition["id"]+hit["_source"]["host"]) == True ):
			log(condition['message'] % hit["_source"]  +  config['config']['link'] % hit["_source"] )

if __name__ == '__main__':

	configFile = "alert.config"
	try :
		configFile = sys.argv[1]
	except Exception, e:
		print "No input parameters - configuration is loaded from 'alert.config'" 

	try:
	
		logging = None
		config = None
		fl = None
		try:
			fl = open ( configFile, "r+")
			config = json.load(fl)
		except Exception, e:
			log ("Cannot open configuration file %s" % e)
			pass

		finally:
			if fl is not None:
				fl.close()
			es = Elasticsearch([config['elasticsearch']])
			
			for rule in config['rules']:
				check_alert(rule)

	except Exception, e:
		log("Monitoring infrastructure on it.devmetrics.io failed with exception: %s"  % e )

