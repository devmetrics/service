#!/bin/bash
TSUNGLOG=/home/ubuntu/tsung/log;
count=0;
while [ $count -lt 10 ];
do
        dir=$(ls $TSUNGLOG  -t | head -n 1);
        echo $dir
        cd $TSUNGLOG/$dir
        perl /usr/lib/tsung/bin/tsung_stats.pl;
        sleep 5;
        ((count++))
done