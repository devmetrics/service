import sys
import json
import re
from mixpanel import Mixpanel

demo_token = 'dc52796a101201cdc10b4c766cb84e83' # demo account
#demo_token = '6ab9ce8186f7580cbd8e9951a99a18d0' # production account

def parse_line(line, p):
	line.strip()
	
	m = p.search(line)

	try: 
		app_id = m.group('token')
		time = m.group('time')
		ip = m.group('ip')
		# agent = m.group('agent')
		#app_id = 'token-123128'
		# agent = 'ttt'
		body = m.group('body')
		body = body.decode('string_escape')
		ob = json.loads(body)
		#if ('.kibana' == ob['docs'][0]['_index']):
		if ('dashboard' == ob['docs'][0]['_type']) :
			post_event(ip, time, app_id, ob['docs'][0]['_id'])
	except Exception, e:
		pass



def post_event(ip, time, app_id, dashboard_name):
    print ip, time, app_id, dashboard_name
    mixpanel = Mixpanel(demo_token)
    #mixpanel.track(app_id, dashboard_name)
    obj = {'ip':ip,'time':time}
    print obj
    try:
    	mixpanel.track(app_id,'dashboard' + '-' + dashboard_name, obj)
    except Exception, e:
    	print e
    
if __name__ == '__main__':
    
    # You'll want to change this to be the token
    # from your Mixpanel project. You can find your
    # project token in the project settings dialog
    # of the Mixpanel web application

    try :
    	demo_token = sys.argv[1]
    except:
    	print "No input parameters - Mixpanel demo account is using"

    #p = re.compile(r'.*\/apps\/(?P<token>[^\/]*)\/elasticsearch\/(?P<dashboard>[^?]*).*')
    #p = re.compile(r'.*POST.*--BODY:"(?P<body>.*)"$')
	#p = re.compile(r'^(?P<ip>[^-].*)--\[(?P<time>[^\]]*).*\/apps\/(?P<token>[^\/]*)\/.*--BODY:"(?P<body>.*)"$')
	#p = re.compile(r'^(?P<ip>[\.0-9]*).*\[(?P<time>[^\]]*).*\/apps\/(?P<token>[^\/]*)\/.*--BODY:"(?P<body>.*)"$')
    p = re.compile(r'^(?P<ip>[\.0-9]*).*\[(?P<time>[^\]]*).*\/apps\/(?P<token>[^\/]*)\/[^\"]*"[^\"]*"[^\"]*" "(?P<agent>.*)" --BODY:"(?P<body>.*)"')
	

    while 1:
	    
	    try:
	        line = sys.stdin.readline()
	        if not line:
	            break
	        parse_line(line, p)
	    except Exception, e:
	        raise e





