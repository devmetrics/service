#!/bin/bash

#Update Sources configuration

SERVERNAME=service.devmetrics.io

git pull
echo
echo "Sources has been updated from git:https://vladimir_devmetrics@bitbucket.org/devmetrics/service.git"
echo " if there are no repository, clone it: git clone https://vladimir_devmetrics@bitbucket.org/devmetrics/service.git "

#Update Logstash configuration
sudo cp -v ./logstash/logstash.apps.conf /etc/logstash/conf.d/
sudo cp -v ./logstash/template.apps.json /etc/logstash/template.apps.json

#experiments with influxdb

sudo cp -v ./logstash/$SERVERNAME/influxdb_internal.conf /etc/logstash/conf.d/

echo
echo "Logstash configuration has been copied if need restart: sudo service logstash restart"
echo
#Update NGINX configuration
sudo cp -v ./config/nginx/$SERVERNAME /etc/nginx/sites-available/
sudo ln -s /etc/nginx/sites-available/$SERVERNAME /etc/nginx/sites-enabled/
echo
echo "NGINX configuration has been copied if need restart: sudo service nginx restart"
echo

cd api
pm2 start server.js
pm2 save
cd ..

echo "Check that service is working"
curl -XGET localhost/api/apps/token-1/

# Elastic Search API 
sudo cp -v ./config/nginx/elasticsearch /etc/nginx/sites-available/
sudo ln -s /etc/nginx/sites-available/elasticsearch /etc/nginx/sites-enabled/
# define new log format access_body
sudo cp -v ./config/nginx/nginx.conf /etc/nginx/
echo
echo "Elastic Serach endpoint has been added to NGINX configuration if need restart: sudo service nginx restart"
echo

echo "Update cron tasks"
cp -v ./cron/$SERVERNAME/* ./cron/
bash ./cron/create_cron_task.sh

#experiments with collectd

sudo cp -v ./collectd/$SERVERNAME/collectd.conf /etc/collectd/

echo
echo "Collectd configuration has been copied if need restart: sudo service collectd restart"
echo

sudo cp -v ./watcher/$SERVERNAME/logtail.sh ./watcher/

echo
echo "watcher configuration copied"
echo




